'use strict';

module.exports = function(User) {
// Otp Send Api
    User.remoteMethod('signin',{
        accepts: [{arg: 'data', type: 'object', 'http': {source: 'body'}, required: true},
        {arg: 'ctx', type: 'object', 'http': {source: 'context'}, required: true}],
        returns: {arg: 'response', type: 'object', root: true},
        http: {path: '/signin', verb: 'post'}
      });
// Login Api
      User.remoteMethod('validateLogin',{
        accepts: [{arg: 'data', type: 'object', 'http': {source: 'body'}, required: true},
        {arg: 'ctx', type: 'object', 'http': {source: 'context'}, required: true}],
        returns: {arg: 'response', type: 'object', root: true},
        http: {path: '/validateLogin', verb: 'post'}
      });



    // User.beforeRemote('login',(context, data, callback) =>  {
    //     console.log('-------')
    // User.find({where: {username: }})
    // next()
    // })

User.signin = (data, ctx, signinCB)=>{
    console.log("-----1111111111---")
    User.findOne({where: {
        or: [
           { phone: data.username },
           { email: data.username}
        ]}}, (err,response)=>{
        if (err) {
            return signinCB(err)
        }
        else {
             // var otp =Math.floor(100000 + Math.random() * 900000)
             var otp = 123456
            if (response) {
                response.updateAttribute('otp', otp)
                response.updateAttribute('isVerified', false)
                response.updateAttribute('modifiedAt', new Date())
                removeAccessToken(response.id)
                signinCB(null, {'message': 'Otp sent successfully'})
            }
            else {
                
                var request = {
                    email: data.isemail === true ? data.username : '',
                    phone: data.isemail === false ? data.username : '',
                    otp: otp,
                    isVerified: false
                }
                User.create(request, (error,userResponse)=> {
                    if (error) {
                        return signinCB(err)  
                    }
                    if (userResponse) {
                        signinCB(null, {'message': 'Otp sent successfully'})
                    }
                })
            }
        }
    })
}

User.validateLogin = (data,context,LoginCB) => {

    User.findOne({ where: {
        or : [
          {email:data.username},
          {phone : data.username}
        ]
    }},(err,result)=>{
    if(err){
        LoginCB(err)
    }else if(result){
     if(data.otp === result.otp){
        result.updateAttribute('otp', null)
        result.updateAttribute('isVerified', true)
        User.app.models.accesstoken.create({
            scopes: ["common-user"],
            created: new Date(),
            userid: result.id
        },(err,token)=>{
            result.token = token.id
            LoginCB(null, result) 
        }) 
     }
     else {
        let error =new Error("Invalid OTP");
        error.status = 403;
        return LoginCB(error);
     }

    }

    })

}

    const removeAccessToken = (userId, removeCB) => {
        User.app.models.accesstoken.deleteAll({
            userid: userId
        }, removeCB);
      }
};
